\input{preamble}
\usepackage{tikzit}
\input{figures/styles.tikzstyles}

\title{ C4 - 2e principe de la thermodynamique }

\begin{document}
	%\includecourse

	\maketitle

	\section{Entropie}%
	
	\ctikzfig{c4-1-1}

	Égalité de Clausius :
	\begin{align*}
		\oint_{cycle} \frac{\delta Q_{rev}}{T}=0
	\end{align*}

	Valable pour tout cycle réversible et pour tout type de matière avec 
	\begin{itemize}
		\item $\delta Q_{rev}$ : chaleur reçue au cours d'une transformation
			infinitésimale $i-k$

		\item $T$ : température au point $i$
	\end{itemize}
	
	Vérification pour un gaz parfait :

	1er principe pour une transformation infinitésimale et réversible :
	\begin{align*}
		&dU= \delta W_{ext\to gaz}+\delta Q_{ext\to gaz} \\
		\donc& \nu c_v dT = -PdV+\delta Q_{rev}
		\shortintertext{$PV=\nu RT$ :}
		\donc& \nu c_v \frac{dT}{T}=- \nu R \frac{dV}{V}+\frac{\delta Q_{rev}}{T}\\
		\donc& \oint_{cycle} \nu c_v \frac{dT}{T}=- \oint_{cycle} \nu R \frac{dV}{V}+
		\oint_{cycle} \frac{\delta Q_{rev}}{T}\\
		\donc& \underbrace{\nu c_v \int_{T_1}^{T_1} \frac{dT}{T}  }_{=0} = \underbrace{-\nu R
		\int_{v_1}^{v_1} \frac{dv}{v}}_{=0} + \oint_{cycle} \frac{\delta Q_{rev}}{T} \\
		\donc& \oint_{cycle} \frac{\delta Q_{rev}}{T}=0
	\end{align*}

	Soit $\frac{\delta Q_{rev}}{T}=dS$ 

	$\oint_{cycle} \frac{\delta Q_{rev}}{T}=\oint_{cycle} dS=0$ d'où $\Delta
	S_{cycle}=0$ 

	$S$ est une fonction d'état : 
	\begin{align*}
		S= \int \frac{\delta Q_{rev}}{T} + c
	\end{align*}
	Elle est appelée entropie.

	Variation de l'entropie au cours d'une transformation réversible.
	\begin{align*}
		\Delta S = S_2-S_1 = \int_{1}^{2} \frac{\delta Q_{rev}}{T} 
	\end{align*}
	Unité SI : $[S] = \frac{[Q]}{[T]}=\frac{J}{K}$ 

	\begin{expl}
		\begin{enumerate}
			\item Evaporation de l'eau

			Ordre / Désordre : 

			Liquide : réseau cristallin "tordu". État plus ordonné

			Vapeur : aucune organisation spatiale. État plus désordonné

			Variation de l'entropie :
			\begin{align*}
				\Delta S = \int_{1}^{2} \frac{\delta
				Q_{rev}}{\underbrace{T}_{=\SI{373.15}{\kelvin}}}=\underbrace{\frac{Q_{\text{evaporation}}}{T}}_{>0} 
			\end{align*}
			Donc $S_{fin}>S_{ini}$ 

			\item Solidification de l'eau

			Ordre / Désordre :

			Liquide : réseau cristallin "tordu". État plus désordonné

			Solide : réseau cristallin. État plus ordonné

			Variation de l'entropie :
			\begin{align*}
				\Delta S &= \int_{1}^{2} \frac{\delta
				Q_{rev}}{\underbrace{T}_{\SI{273.15}{\kelvin}}} =
				\underbrace{\frac{Q_{solidification}}{T}}_{Q<0}
			\end{align*}
			Donc $S_{fin}<S_{ini}$

		\end{enumerate}
	\end{expl}

	\begin{defi}
		L'entropie mesure le degré de désordre d'un système. Plus l'entropie est élevée,
		moins les éléments du système sont ordonnés, liés entre eux.
	\end{defi}

	\begin{fml}
		\begin{align*}
			S= \int \frac{\delta Q_{rev}}{T} + c
		\end{align*}
	\end{fml}

	\section{Transformations réversibles}%
	\label{sec:transformations_reversibles}
	
	\subsection{Définition}%
	\label{sub:definition}
	
	\begin{defi}
		La transformation thermodynamique est dite réversible lorsque le chemin de la
		transformation inverse coincide avec le chemin de la transformation directe.

		La transformation inverse ramène ainsi le système dans son état initial.

		La transformation réversible n'est possible qu'en absence de frottements dans le
		système.
	\end{defi}

	\ctikzfig{c4-2-1}

	\subsection{Diagramme TS}%

	\begin{center}
		\tikzfig{c4-2-2}
		\tikzfig{c4-2-3}
	\end{center}

	\begin{itemize}
		\item 
		On étudie la transformation infinitésimale réversible $i-k$. Variation de l'entropie
		$dS = \frac{\delta Q_{rev}}{T}$ 

		Chaleur élémentaire reçue : $\delta Q_{rev}=T dS$ 

		Sur le diagramme TS : $\abs{ \delta Q_{rev} }=Aire[CikD]$ 

		\item On étudie la transformation réversible 1-2

		Chaleur reçue : $Q_{rev}=\int_{1}^{2} \delta Q_{rev}=\int_{S_1}^{S_2} T( S )dS$ 

		Sur $TS$ : $\abs{ Q_{rev} }=Aire[A12B]$
	\end{itemize}
	La valeur absolue de la chaleur reçue représente l'aire au dessous de la courbe $1-2$ 
	sur le diagramme TS.

	\subsection{1er principe de la thermodynamique pour des transformations réversibles}%
	
	Pour une transformation infinitésimale réversible :
	\begin{align*}
		\left\{
		\begin{NiceMatrix}
			dU = \delta W_{ext\to sys}+\delta Q_{ext\to sys}\\
			\delta W_{ext\to sys}=-PdV \\
			\delta Q_{ext\to sys}=TdS
		\end{NiceMatrix}
		\right. \implies dU = -PdV+TdS
	\end{align*}
	D'où :
	\begin{fml}
		1er principe pour transformations réversibles :
		\begin{align*}
			TdS = dU + PdV
		\end{align*}
	\end{fml}
	\begin{rmq}
		\begin{align*}
			\delta Q_{rev}&=TdS\\
			\delta Q_{irrev} &\neq TdS
		\end{align*}
	\end{rmq}

	\subsection{Variation de l'entropie des gaz parfaits, liquides, solides incompressibles}%
	
	\ctikzfig{c4-2-4}
	
	On cherche 
	\begin{align*}
		\Delta S &= S_2-S_1 \\
				 &= f_1( T_1,T_2, V_1,V_2 ) \\
				 &= f_2( T_1,T_2,P_1,p_2 )
	\end{align*}

	\textbf{Cherchons $f_2$}

	On utilise le 1er principe : 
	\begin{align}
		TdS &= dU + PdV\\
		dU &= \nu c_v dT\\
		PV &= \nu RT \implies P = \frac{\nu RT}{V}
	\end{align}

	D'où :
	\begin{align*}
		dS &= \nu c_v \frac{dT}{T}+\nu R \frac{dV}{V} \\
		\Delta S &= \int_{S_1}^{S_2} dS  \\
		&= \int_{T_1}^{T_2} \nu c_v \frac{dT}{T}+ \int_{V_1}^{V_2} \nu R \frac{dV}{V}   \\
		&= \nu c_v \frac{T_2}{T_1}+\nu R \ln\left(\frac{V_2}{V_1}\right)
	\end{align*}

	\textbf{Cherchons $f_1$}

	On exprime $PdV$ en fonction de dP :
	\begin{align}
		d( PV )=PdV+VdP \implies PdV=d( PV )-VdP
	\end{align}
	On déduit de $( 1 ), ( 4 )$ :
	\begin{align*}
		TdS&= \underbrace{dU+d( PV )}_{d( U+PV )}-VdP \numberthis \\
		dH &= \nu c_p dT  \numberthis\\
		V &= \frac{\nu RT}{P} \numberthis \\
		\shortintertext{On déduit de $( 6 ),( 7 ),( 5 )$ : }
		TdS &= \nu c_p dT - \frac{\nu RT}{P}dP\\
		\implies dS &= \nu c_p \frac{dT}{T}-\frac{\nu R}{P}dP \\
		\implies \Delta S &= \int_{T_1}^{T_2} \nu c_p \frac{dT}{T}- \int_{P_1}^{P_2} \nu R
		\frac{dP}{P}\\
		&= \nu c_p \ln\left(\frac{T_2}{T_1}\right) -\nu R \ln\left(\frac{P_2}{P_1}\right)
	\end{align*}

	\begin{fml}
		\textbf{Gaz parfait :} 
		\begin{align*}
			\Delta S =
			\left\{
			\begin{NiceMatrix}
				\nu c_v \ln\left(\frac{T_2}{T_1}\right) + \nu R
				\ln\left(\frac{V_2}{V_1}\right)\\
				\nu c_v \ln\left(\frac{T_2}{T_1}\right) - \nu R
				\ln\left(\frac{P_2}{P_1}\right)
			\end{NiceMatrix}
			\right.
		\end{align*}
	\end{fml}

	Pour liquide, solide incompressible :
	\begin{align*}
		( 1 )\implies TdS &= dU + P \underbrace{dV}_{\simeq 0} \simeq dU = \nu c_v dT = M
		c_m dT\\
		\implies dS &= M c_m \frac{dT}{T}\\
		\implies \Delta S &= \int_{T_1}^{T_2} Mc_m \frac{dT}{T}\\
						  &= M c_m \ln\left(\frac{T_2}{T_1}\right)
	\end{align*}
	\begin{fml}
		\textbf{Solide, liquide incompressible :} 
		\begin{align*}
			M c_m \ln\left(\frac{T_2}{T_1}\right)
		\end{align*}
		Avec $M$ masse, $c_m$ capacité thermique massique en
		$\SI{}{\joule\per\kelvin\per\kilo\gram}$
	\end{fml}

	\section{Transformations irréversibles, systèmes isolés}%
	
	\subsection{Classification des systèmes}%
	
	Il y a trois types de système :
	\begin{itemize}
		\item Système isolé : aucun échange, $Q=W=0$ 
		\item Système thermiquement isolé : $Q=0, W\neq 0$. Exemple : piston
		\item Système non isolé : $Q\neq 0,W\neq 0$
	\end{itemize}

	\subsection{Mélange de deux gaz parfaits}%
	
	Transformation directe. On a un système contenant deux sous-systèmes, séparés par une
	paroi :
	\begin{itemize}
		\item $O_2$ : $\nu_1, P_1,V_1,T_1$
		\item $N_2$ : $\nu_1, P_1,V_1,T_1$
	\end{itemize}

	La paroi rompt. On obtient un système final avec $\nu_1 + \nu_2, P_f, 2V, T_f$ 

	On a $Q=0, W=0$. C'est donc un système isolé.

	On a : concentration $N_2, O_2$ hétérogène $\to$ concentration $N_2,O_2$ homogène

	état hors équilibre $\to$ état d'équilibre

	La transformation inverse, à l'issue de laquelle toutes les molécules $O_2$ se
	déplaceraient vers la gauche et toutes les molécules $N_2$ se déplaceraient vers la
	droite est impossible sans qu'on fournisse du travail ou de la chaleur au système.

	Le système ne reviendra jamais spontanément dans son état initial.

	\textbf{Retour forcé à l'état initial} 

	état final $\to $ liquéfaction $N_2+O_2, Q\neq 0$ $\to $ centrifugation + remise cloison
	$W\neq 0$ $\to $ évaporation $N_2$ et $O_2$ $\to $ état initial
 
	Donc, retour par un autre chemin que le chemin d'aller

	\begin{defi}
		Une transformation thermodynamique est appelée irréversible si le système ne peut
		pas revenir à son état initial par le même chemin. Le chemin de la transformation
		inverse ne coincide pas avec le chemin de la transformation directe.
	\end{defi}

	\textbf{Étude de la transformation directe (mélange)} 

	1) 1er principe 
	\begin{align*}
		\Delta U &= W_{ext\to sys}+Q_{ext\to sys}=0\\
		\Delta U &= \Delta U_{O_2}+\Delta U_{N_2} = \underbrace{\nu c_v( T_f-T_i )}_{O_2}
		+ \nu c_v ( T_f-T_i )=0\\
		\implies T_f = T_i
	\end{align*}

	2) Variation de l'entropie de $O_2+N_2$ 
	\begin{align*}
		\Delta S &= \Delta S_{O_2} + \Delta S_{N_2}\\
				 &= \nu c_v \ln\left(\frac{T_f}{T_i}\right)+\nu R
				 \ln\left(\frac{V_f}{V_i}\right) + \nu c_v
					 \ln\left(\frac{T_f}{T_i}\right) + \nu
					 \ln\left(\frac{V_f}{V_i}\right)\\
				 &= 2 \nu R \ln(2) > 0
	\end{align*}

	Lors du mélange des deux gaz, le système passe d'un état ordonné vers un état
	désordonné. L'entropie du système augmente.

	\subsection{Énoncé du 2e principe pour des systèmes isolés}%
	
	État initial : deux gaz séparés dans un système.

	État final : deux gaz mélangés

	$P,T,n$ hétérogènes deviennent homogènes

	État : hors équilibre devient équilibre. Ordonné devient désordonné

	$S_{ini} < S_{fin} \implies S \nnearrow$

	Un système thermodynamique isolé hors équilibre évolue toujours vers un état
	d'équilibre pour lequel toutes les variables d'état sont stables. Une fois à l'équilibre, le
	système isolé ne peut jamais évoluer spontanément vers un état hors équilibre. Toute
	évolution du système isolé est absolument irréversible et s'accompagne toujours d'une
	augmentation de son entropie.
	\begin{defi}
		\begin{align*}
			\Delta S > 0 \text{ pour systèmes isolés}
		\end{align*}
	\end{defi}

	Énoncé de Clausius : La chaleur ne passe pas spontanément d'un corps plus froid à un
	corps plus chaud

	\section{Systèmes thermiquement isolés}%

	\subsection{Transformations adiabatiques réversible et irréversible}
	
	\subsubsection{Compression adiabatique (assez) lente}%
	
	\begin{enumerate}
		\item Réalisation / équilibre

		\ctikzfig{c4-4-1}

		$P,T,n$ restent homogènes à tout instant de compression. $PV^\gamma = const$

		\item Travail reçu
		\begin{align*}
			W_{ext\to gaz} &= \int \vec{ F_{ext} } \cdot \vec{ dr } = \ldots =
			-\int_{V_1}^{V_2} P_{ext}( V )dV \\
			\shortintertext{On a :}
			\underbrace{P_{ext}}_{\text{pression externe}} = \frac{F_{ext}}{S} =
			\underbrace{P}_{\text{pression interne}}
			\shortintertext{D'où :}
			W_{ext\to gaz} &= -\int_{V_1}^{V_2} P( V )dV  
		\end{align*}
		$P_{ext}$ aller $>P_{ext}$ retour donc $\abs{ W }$ aller $> \abs{ W }$ retour

		\item Premier principe

		$\Delta U_{aller}=\nu c_v ( T_2-T_1 )=W_{aller}>0$

		\begin{align*}
			\abs{ W_{aller} } > \abs{ W_{retour} } \implies& \abs{ \Delta U_{aller} } >
			\abs{\Delta U_{retour}}\\
			\implies& \abs{ \underbrace{T_2-T_1}_{>0} }>\abs{ \underbrace{T_3-T_2}_{<0} }\\
			\implies& T_2-T_1 > T_2-T_3\\
			\implies& T_3>T_1\\
			\shortintertext{Comme :}
			\left.
			\begin{NiceMatrix}
				P_3=P_1 \simeq P_0\\
				T_3 > T_1\\
				\nu = const
			\end{NiceMatrix}
			\right\} \alors V_3 >V_1
		\end{align*}
		Au bout d'un aller retour, le gaz n'arrive pas à son état initial

		La compression lente est réversible.

		\item Variation de l'entropie

		Par définition $dS_{rev} = \frac{\delta Q_{rev}}{T}=0$ 

		Et donc $S_{rev}=0$ 

		En même temps, $\Delta S_{rev}=\nu c_v \ln\left(\frac{T_2}{T_1}\right)+\nu R
		\ln\left(\frac{V_2}{V_1}\right)$

		On prouve qu'avec $P_1V_1\gamma=P_2V_2\gamma$ 

		On a $\Delta S_{rev}=0$ 

		La variation de l'entropie au cours d'une transformation adiabatique réversible est
		nulle et cette transformation est appelée isentropie
	\end{enumerate}

	\subsubsection{Détente adiabatique brutale}%
	
	\begin{enumerate}
		\item Réalisation / équilibre

		\ctikzfig{c4-4-2}

		On relâche la pression et le gaz se détend brutalement

		$P,T,n$ au fond $> P,T,n$ près du piston

		\item Travail reçu
		\begin{align*}
			W_{ext\to gaz}&= -\int_{V_1}^{V_2} P_{ext}( V ) dV \\
			\shortintertext{Or :}
			P _{ext} = \frac{F_{ext}}{S} \neq P\\
			\shortintertext{D'où :}
			W_{ext\to gaz} \neq - \int_{V_1}^{V_2} P( V )dV 
		\end{align*}

		\item Premier principe

		$\Delta U_{retour}=\nu c_v ( T_3-T_2 )=W_{retour}<0$

		\ctikzfig{c4-4-3}

		La détente brutale est irréversible

		\item Variation de l'entropie

		$dS_{irr}\neq \frac{\delta Q_{irr}}{T}$ mais toujours valable :

		$\Delta S_{irr}=\underbrace{\nu c_v \ln\left(\frac{T_2}{T_1}\right)}_{<0}+\underbrace{\nu R
		\ln\left(\frac{V_3}{V_2}\right)}_{>0}$
		\begin{align*}
			\Delta S_{irr}&= \Delta S_{irr}+\underbrace{\Delta S_{rev}}_{=0} \\
			&= \nu c_v \ln\left(\frac{T_3}{T_2}\right) + \nu R
			\ln\left(\frac{V_3}{V_2}\right) + \nu c_v \ln\left(\frac{T_2}{T_1}\right) +
			\nu R \ln\left(\frac{V_2}{V_1}\right) \\
			&= \nu c_v \ln\left(\frac{T_3}{T_1}\right) + \nu R
			\ln\left(\frac{V_3}{V_1}\right) > 0
		\end{align*}
		Au cours de la transformation adiabatique irréversible, l'entropie ne peut
		qu'augmenter

		\ctikzfig{c4-4-4}
	\end{enumerate}
	
	\subsection{Résumé pour systèmes thermiquement isolés}%

	\subsubsection{Transformation réversible}%
	\label{ssub:transformation_reversible}
	
	\begin{enumerate}
		\item Infiniment (en théorie) lente
		\item Pas de frottements 
		\item $P,T,n$ homogènes
		\item Travail reçu : $W_{ext\to gaz}=-\int_{V_1}^{V_2} \underbrace{P_{ext}}_{=P}( V )dV $
		\item Il existe l'équation de transfo $P=f( V )$. Ex : $PV^\gamma=const$
		\item 
		\begin{fml}
			Égalité de Clausius :
			\begin{align*}
				\Delta S = \int_{1}^{2} \frac{\delta Q_{rev}}{T} 
			\end{align*}
		\end{fml}
		\item $\Delta S = 0, S = const$
	\end{enumerate}

	\subsubsection{Transformation irréversible}%
	\label{ssub:transformation_irreversible}
	
	\begin{enumerate}
		\item Vitesse finie
		\item Présence de frottements
		\item $P,T,n$ hétérogènes
		\item Travail reçu : $W_{ext\to gaz}=-\int_{V_1}^{V_2} P_{ext}( V )dV $
		\item Il n'existe pas d'équation de transfo $P=f( V )$
		\item 
		\begin{fml}
			Inégalité de Clausius :
			\begin{align*}
				\Delta S > \int_{1}^{2} \frac{\delta Q_{irr}}{T} 
			\end{align*}
		\end{fml}
		\item $\Delta S > 0, S  \nnearrow$
	\end{enumerate}
	
	\section{Systèmes non-isolés. Création de l'entropie}%
	
	\textbf{Exemple : Solidification de l'eau} 

	Système adiabatique. 

	État initial : On place un verre d'eau dans le système. Sa température ne varie pas.
	
	État final : L'eau est devenue de la glace. 

	\begin{enumerate}
		\item Système = eau = système non isolé
		\begin{align*}
			\Delta S_{eau} = \int \frac{\delta Q}{T} = \frac{Q_{air\to
			eau}}{T}=-\frac{\abs{ Q }}{T} < 0
		\end{align*}

		L'entropie d'un système non isolé peut diminuer et le système peut devenir plus
		ordonné.

		\item Système = air du congélateur = système non isolé
		\begin{align*}
			\Delta S_{air}=\int \frac{\delta Q}{T} = \frac{Q_{eau\to air}}{T_0} =
			\frac{\abs{ Q }}{T_0} >0
		\end{align*}
		\item Système air + eau = système isolé
		\begin{align*}
			\Delta S_{tot}=S_{air}+S_{eau}= - \frac{\abs{ Q }}{T}+\frac{\abs{ Q }}{T_0} >0
		\end{align*}
		Donc $\Delta S_{tot}>0$ et $\abs{ \Delta S_{air} }>\abs{ \Delta S_{eau} }$

		L'entropie totale d'un système avec son milieu extérieur ne peut qu'augmenter. Ainsi
		l'ensemble "système + extérieur" devient moins ordonné, quelque soit le processus
		ayant lieu dans le système

		\item Système eau : système non isolé

		$\Delta S_{eau}=\Delta S_{tot}-\Delta S_{air}$, avec $\Delta S_{air}=\frac{Q_{eau\to
		air}}{T_0}=- \frac{Q_{air\to eau}}{T_0}$.

		D'où : $\Delta S_{eau}=\Delta S_{tot}+\frac{Q_{air\to eau}}{T_0}$ 

		$\Delta S_{tot}$ correspond à l'augmentation de l'entropie dans un système avec
		son milieu extérieur. \textbf{Entropie créée}.

		$\frac{Q_{air\to eau}}{T_0}$ correspond à l'échange thermique entre système et
		milieu extérieur. \textbf{Entropie d'échange} 
	\end{enumerate}

	\begin{defi}
		\textbf{Énoncé du deuxième principe pour des systèmes non isolés}

		La variation de l'entropie d'un système non isolé peut être positive, négative, ou
		nulle. Elle présente une somme de l'entropie d'échange et de l'entropie créée au
		cours de la transformation considérée.
	\end{defi}

	\begin{fml}
		\begin{align*}
			\Delta S = S_{fin}-S_{ini}=S_{échange}+S_{créée}\\
			S_{échange}= \int_{ini}^{fin} \frac{\delta Q_{ext\to sys}}{T_{ext}} 
		\end{align*}
	\end{fml}

	\section{Moteurs thermiques}%
	\label{sec:moteurs_thermiques}

	\subsection{Cycle de Carnot : cycle de référence}%
	
	\begin{center}
		\tikzfig{c6-1-1}
		\tikzfig{c6-1-2}
	\end{center}

	Premier principe pour le cycle :
	\begin{align*}
		\underbrace{\Delta U_{cycle}}_{0}&= W_{cycle}+Q_{cycle}=0\\
		\iff& Q_{cycle}=-W_{cycle} \ou \abs{ Q_{cycle} }=\abs{ W_{cycle} } \numberthis
	\end{align*}
	Chaleur du cycle : 
	\begin{equation}
		\abs{ Q_{cycle} }=\underbrace{Q_1}_{>0}-\underbrace{Q_2}_{>0}
	\end{equation}
	Avec $Q_1$ chaleur de source chaude, $Q_2$ chaleur évacuée vers source froide

	Rendement du cycle :
	\begin{align*}
		\eta_{th}&= \frac{\text{effet utile}}{\text{effet couteux}} \\
		&= \frac{\abs{ W_{cycle} }}{Q_1} \\
		&= \frac{Q_1-Q_2}{Q_1} \\
		&= 1- \frac{Q_2}{Q_1} \numberthis
	\end{align*}

	Pour Carnot :
	\begin{align*}
		Q_1&=Aire[A12B]= T_I( S_B-S_A )\\
		Q_2&=Aire[A43B]=T_{II}( S_B-S_A )
	\end{align*}
	On déduit de $( 3 )$ :
	\begin{equation}
		\eta_{carnot} = 1- \frac{T_{II}}{T_I} < 1
	\end{equation}

	\subsection{}%
	
	\begin{itemize}
		\item Schéma : \ctikzfig{c6-1-3}

		$Q_{1, réel}=Aire[E123F]$ <	$Q_{1, carnot}=Aire[EABF]$

		$Q_{2, réel}=Aire[E143F]$ > $Q_{2, carnot}=Aire[EDCF]$

		$\frac{Q_{2, réel}}{Q_{1,réel}} > \frac{Q_{2,carnot}}{Q_{1, carnot}}$

		$1-\frac{Q_{2, réel}}{Q_{1,réel}} < 1-\frac{Q_{2,carnot}}{Q_{1, carnot}}$

		\item Finalement : $\eta_{th, réel} < \eta_{th, carnot}=1-\frac{T_{II}}{T_I} < 1$
	\end{itemize}

	\begin{defi}
		\textbf{Énoncés alternatifs des deux premiers principes} 

		\textbf{1er principe :} aucune machine thermique ne peut produire plus d'énergie
		(sous forme de travail) qu'elle en consomme

		\textbf{2e principe :} aucune machine thermique ne peut produire plus d'énergie
		que la machine de Carnot
	\end{defi}

	\subsection{}%
	
	Schéma : cycle à 4 étapes. Dans le sens des aiguilles d'une montre, en partant du coin
	inférieur gauche : $1234$.

	Rectangle légèrement incliné. Son aire est égale à $\abs{ Q_{cycle} }=\abs{ W_{cycle} }$ 

	Entre $2$ et $3$, $V$ est constant. La combustion a lieu, augmentation de la
	température. C'est $Q_1$

	Entre $4$ et $1$, $V$ est constant. Atmosphère froide, refroidissement. C'est $Q_2$
	
	Chaleur reçue au cours de la combustion à $V$ const :
	\begin{align*}
		Q_1&= \abs{ \nu c_v ( T_3-T_2 ) }=\nu c_v ( T_3-T_2 )
	\end{align*}
	Chaleur cédée à l'atmosphère à $V$ const.
	\begin{align*}
		Q_2&= \abs{ \nu c_v ( T_1-T_4 ) }=\nu c_v ( T_4-T_1 )
	\end{align*}
	Rendement thermique :
	\begin{align*}
		\eta_{th} &= 1-\frac{Q_2}{Q_1}=1-\frac{1}{\epsilon^{\gamma -1}}\\
		\shortintertext{Avec :}
		\epsilon &= \frac{V_1}{V_2} \text{ taux de compression}\\
		\gamma & \text{ exposant adiabatique}
	\end{align*}

	\subsection{}%
	
	Même schéma que le précédent, mais entre 2 et 3, la pression est constante au lieu de
	la température.

	Chaleur reçue au cours de la combustion à $P$ const :
	\begin{align*}
		Q_1&= \abs{ \nu c_p ( T_3-T_2 ) }=\nu c_p ( T_3-T_2 )
	\end{align*}
	Chaleur cédée à l'atmosphère à $P$ const.
	\begin{align*}
		Q_2&= \abs{ \nu c_v ( T_1-T_4 ) }=\nu c_v ( T_4-T_1 )
	\end{align*}
	Rendement thermique :
	\begin{align*}
		\eta_{th} &= 1-\frac{Q_2}{Q_1}=1-\frac{c_v ( T_4-T_1 )}{c_p( T_3-T_2 )}\\
				  &= \frac{T_4-T_1}{\gamma ( T_3-T_2 )}
	\end{align*}

\end{document}
